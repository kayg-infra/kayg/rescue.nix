{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    git
    tmux
  ];

  services.openssh = {
    enable = true;
    ports = [ 895 ];
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIITI12j3R3vIbY/auH79D01lxmHP4EnryIrosrP3++68 kayg@ruri"
  ];
}
